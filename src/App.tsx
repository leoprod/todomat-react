import React from 'react';
import logo from './logo.svg';
import './App.scss';
import Firestore from './services/Firestore';
const appName = 'ToDomat';

function App() {

  function addItem() {
    const fs = new Firestore();
    fs.createFireStoreItem({
          title: 'React Item from The Server',
          description: 'React is in business again from the new configuration file',
          duration: 25,
          rest: 5
        });
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          {appName} using <code>React</code>.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
          <div className="alert success">
              <img src="https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Tick_Mark_Dark-24.png" alt="success icon"/>
              <span>Bitbucket pipelines work</span>
          </div>
        <hr/>
        <button onClick={addItem}>Add new item</button>
      </header>
    </div>
  );
}

export default App;
