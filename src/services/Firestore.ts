// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";
import "firebase/firestore";
import firebaseConfig from "../firebaseConfig.json";

firebase.initializeApp(firebaseConfig);

interface Pomodoro {
	title: string;
	description?: string;
	duration: number;
	rest: number;
}

class Firestore {
	db: firebase.firestore.Firestore;

	constructor() {
		 this.db = firebase.firestore();
	}

	async createFireStoreItem(item: Pomodoro): Promise<void> {
		try {
			await this.db
				.collection('pomodoros')
				.doc('2')
				.set(item);
			console.log('React Document successfully written!');
		} catch (error) {
			console.error('Error writing document: ', error);
		}
	}
}

export default Firestore;
